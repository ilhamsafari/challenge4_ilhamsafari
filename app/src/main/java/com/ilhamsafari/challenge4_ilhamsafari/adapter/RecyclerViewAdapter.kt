package com.ilhamsafari.challenge4_ilhamsafari

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.ilhamsafari.challenge4_ilhamsafari.MainActivity
import com.ilhamsafari.challenge4_ilhamsafari.R
import com.ilhamsafari.challenge4_ilhamsafari.data.NoteDataClass
import com.ilhamsafari.challenge4_ilhamsafari.database.NoteDatabase
import com.google.android.material.textfield.TextInputLayout
import kotlinx.android.synthetic.main.recycler_item_data.view.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async

class RecyclerViewAdapter(private val noteData: ArrayList<NoteDataClass> = arrayListOf(), private var returnedCall: (result: Boolean) -> Unit) :
    RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleText: TextView = itemView.findViewById(R.id.item_title)
        val descriptionText: TextView = itemView.findViewById(R.id.item_description)
        val dateText: TextView = itemView.findViewById(R.id.item_date)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val getView = LayoutInflater.from(parent.context).inflate(R.layout.recycler_item_data, parent, false)
        return ViewHolder(getView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.titleText.text = noteData[position].title
        holder.descriptionText.text = noteData[position].description
        holder.dateText.text = noteData[position].date

        // Edit
        holder.itemView.image_button_edit.setOnClickListener { it ->
            val mDb = NoteDatabase.getInstance(holder.itemView.context)

            val objectNote = noteData[position]

            val getView = LayoutInflater.from(it.context).inflate(R.layout.custom_dialog_edit, null, false)
            val dialogBuilder = androidx.appcompat.app.AlertDialog.Builder(it.context)
            dialogBuilder.setView(getView)
            dialogBuilder.setCancelable(true)

            val textTitle = getView.findViewById<TextInputLayout>(R.id.input_item_title)
            val textDescription = getView.findViewById<TextInputLayout>(R.id.input_item_description)
            val textDate = getView.findViewById<TextInputLayout>(R.id.input_item_date)
            textTitle.editText?.setText(objectNote.title)
            textDescription.editText?.setText(objectNote.description)
            textDate.editText?.setText(objectNote.date)

            val customDialog = dialogBuilder.create()
            val saveButton = getView.findViewById<Button>(R.id.save_edit)

            saveButton.setOnClickListener {
                objectNote.title = textTitle.editText?.text.toString()
                objectNote.description = textDescription.editText?.text.toString()
                objectNote.date = textDate.editText?.text.toString()

                if (objectNote.title.isEmpty() or objectNote.description.isEmpty() or objectNote.date.isEmpty()) {
                    Toast.makeText(it.context, "Please Fill All Blanks", Toast.LENGTH_LONG).show()
                } else {
                    GlobalScope.async {
                        val result = mDb?.NoteDao()?.updateNote(objectNote)
                        (holder.itemView.context as MainActivity).runOnUiThread {
                            if (result != 0) {
                                Toast.makeText(holder.itemView.context, "Note ${objectNote.title} Edited Successfully", Toast.LENGTH_LONG).show()
                                customDialog.dismiss()
                            } else {
                                Toast.makeText(it.context, "Note ${objectNote.title} Edit Failed", Toast.LENGTH_LONG).show()
                            }
                        }
                        returnedCall(true)
                    }
                }
            }
            customDialog.show()
        }

        // Delete
        holder.itemView.image_button_delete.setOnClickListener {
            AlertDialog.Builder(it.context).setPositiveButton("Yes") { _, _ ->
                val mDb = NoteDatabase.getInstance(holder.itemView.context)

                GlobalScope.async {
                    val result = mDb?.NoteDao()?.deleteNote(noteData[position])

                    (holder.itemView.context as MainActivity).runOnUiThread {
                        if (result != 0) {
                            Toast.makeText(
                                it.context,
                                "Note ${noteData[position].title} Deleted Successfully",
                                Toast.LENGTH_LONG
                            ).show()
                        } else {
                            Toast.makeText(
                                it.context,
                                "Data ${noteData[position].title} Delete Failed",
                                Toast.LENGTH_LONG
                            ).show()
                        }
                    }
                    returnedCall(true)
                }
            }.setNegativeButton("No") { p0, _ ->
                p0.dismiss()
            }.setMessage("Are You Sure Want To Delete ${noteData[position].title} Note?")
                .setTitle("Are You Sure?").create().show()
        }
    }

    override fun getItemCount(): Int {
        return noteData.size
    }
}