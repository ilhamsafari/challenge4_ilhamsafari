package com.ilhamsafari.challenge4_ilhamsafari

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import com.ilhamsafari.challenge4_ilhamsafari.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding : ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        supportActionBar?.hide()
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    companion object {
        const val SHARED_PREF_FILE = "kotlinsharedpreference"
        const val LOGGED_IN = "loggedin"
        const val LOGGED_USERNAME = "loggedinusername"
        const val LOGGED_IN_USER_ID = "loggedinuserid"
    }
}