package com.ilhamsafari.challenge4_ilhamsafari.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ilhamsafari.challenge4_ilhamsafari.dao.NoteDao
import com.ilhamsafari.challenge4_ilhamsafari.data.NoteDataClass

@Database(entities = [NoteDataClass::class], version = 1)
abstract class NoteDatabase : RoomDatabase() {
    abstract fun NoteDao(): NoteDao

    companion object {
        private var INSTANCE: NoteDatabase? = null

        fun getInstance(context: Context): NoteDatabase? {
            if (INSTANCE == null) {
                synchronized(NoteDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext, NoteDatabase::class.java, "notes.db"
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}