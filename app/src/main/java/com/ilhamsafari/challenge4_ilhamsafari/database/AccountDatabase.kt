package com.ilhamsafari.challenge4_ilhamsafari.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ilhamsafari.challenge4_ilhamsafari.AccountDao

import com.ilhamsafari.challenge4_ilhamsafari.data.AccountDataClass

@Database(entities = [AccountDataClass::class], version = 1)
abstract class AccountDatabase : RoomDatabase() {
    abstract fun AccountDao(): AccountDao

    companion object {
        private var INSTANCE: AccountDatabase? = null

        fun getInstance(context: Context): AccountDatabase? {
            if (INSTANCE == null) {
                synchronized(AccountDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext, AccountDatabase::class.java, "accounts.db"
                    ).build()
                }
            }
            return INSTANCE
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}