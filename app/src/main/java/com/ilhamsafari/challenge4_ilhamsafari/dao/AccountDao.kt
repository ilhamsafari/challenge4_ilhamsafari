package com.ilhamsafari.challenge4_ilhamsafari

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.ilhamsafari.challenge4_ilhamsafari.data.AccountDataClass

@Dao
interface AccountDao {
    @Query("SELECT * FROM AccountDataClass")
    fun getAllAccount(): List<AccountDataClass>

    @Query("SELECT * FROM AccountDataClass WHERE email = :email")
    fun getEmailAccountUnique(email: String) : List<AccountDataClass>

    @Query("SELECT * FROM AccountDataClass WHERE email = :email and password = :password")
    fun getLoginAccount(email: String, password: String) : List<AccountDataClass>

    @Query("SELECT * FROM AccountDataClass WHERE id = :account_id")
    fun getSingleInformationAccount(account_id: Int) : List<AccountDataClass>

    @Query("UPDATE AccountDataClass SET password = :new_password WHERE id = :account_id")
    fun updatePasswordAccount(new_password: String, account_id: Int): Int

    @Query("UPDATE AccountDataClass SET username = :new_username WHERE id = :account_id")
    fun updateUsernameAccount(new_username: String, account_id: Int): Int

    @Query("DELETE FROM AccountDataClass WHERE id = :account_id")
    fun deleteSingleAccount(account_id: Int): Int

    @Insert(onConflict = REPLACE)
    fun insertAccount(account: AccountDataClass): Long

    @Update
    fun updateAccount(account: AccountDataClass): Int

    @Delete
    fun deleteAccount(account: AccountDataClass): Int
}