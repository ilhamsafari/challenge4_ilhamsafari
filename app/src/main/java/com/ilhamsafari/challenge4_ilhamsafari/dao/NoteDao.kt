package com.ilhamsafari.challenge4_ilhamsafari.dao

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.ilhamsafari.challenge4_ilhamsafari.data.NoteDataClass

@Dao
interface NoteDao {
    @Query("SELECT * FROM NoteDataClass")
    fun getAllNote(): List<NoteDataClass>

    @Query("SELECT * FROM NoteDataClass WHERE user_id_create = :user_id")
    fun getAllNoteUser(user_id: Int): List<NoteDataClass>

    @Query("DELETE FROM NoteDataClass WHERE user_id_create = :user_id")
    fun deleteAllNoteUser(user_id: Int): Int

    @Insert(onConflict = REPLACE)
    fun insertNote(note: NoteDataClass): Long

    @Update
    fun updateNote(note: NoteDataClass): Int

    @Delete
    fun deleteNote(note: NoteDataClass): Int
}