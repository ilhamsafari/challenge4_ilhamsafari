package com.ilhamsafari.challenge4_ilhamsafari.data

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class NoteDataClass(
    @PrimaryKey(autoGenerate = true) val id: Int?,
    @ColumnInfo(name = "user_id_create") val user_id_create: Int,
    @ColumnInfo(name = "title") var title: String,
    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "date") var date: String
) : Parcelable
