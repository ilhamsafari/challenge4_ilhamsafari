package com.ilhamsafari.challenge4_ilhamsafari.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.ilhamsafari.challenge4_ilhamsafari.MainActivity
import com.ilhamsafari.challenge4_ilhamsafari.R
import com.ilhamsafari.challenge4_ilhamsafari.data.NoteDataClass
import com.ilhamsafari.challenge4_ilhamsafari.database.AccountDatabase
import com.ilhamsafari.challenge4_ilhamsafari.database.NoteDatabase
import com.ilhamsafari.challenge4_ilhamsafari.databinding.FragmentHomeBinding
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.textfield.TextInputLayout
import com.ilhamsafari.challenge4_ilhamsafari.RecyclerViewAdapter
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import kotlin.system.exitProcess

class HomeFragment : Fragment() {
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private var mDb: NoteDatabase? = null
    private var nDb: AccountDatabase? = null

    // For Testing
    // NoteDataClass(0, "Latihan Binar", "Topic Pertama", "2022-03-29"),
    // NoteDataClass(1, "Latihan Binar", "Topic Kedua", "2022-03-30"),
    // NoteDataClass(2, "Latihan Binar", "Topic Ketiga", "2022-03-31"),
    // NoteDataClass(3, "Latihan Binar", "Topic Keempat", "2022-04-01"),

//    private val listData = arrayListOf<NoteDataClass>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val sharedPreference: SharedPreferences = requireActivity().getSharedPreferences(
            MainActivity.SHARED_PREF_FILE,
            Context.MODE_PRIVATE
        )
        binding.textWelcomeHome.text = "Welcome Back ${sharedPreference.getString(MainActivity.LOGGED_USERNAME, "")}"

        val layoutManager = LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
        binding.homeRecyclerView.layoutManager = layoutManager
        fetchData()

        fun logoutAccount() {
            val alertDialog = AlertDialog.Builder(requireContext())
            alertDialog.setTitle("Are You Sure?")
            alertDialog.setMessage("Are You Sure Want To Logout?")
            alertDialog.setPositiveButton("Yes") { _, _ ->
                val editor: SharedPreferences.Editor = sharedPreference.edit()
                editor.putBoolean(MainActivity.LOGGED_IN, false)
                editor.remove(MainActivity.LOGGED_USERNAME)
                editor.apply()
                view.findNavController().navigateUp()
                Toast.makeText(requireContext(), "Logged Out Successfully", Toast.LENGTH_LONG).show()
            }
            alertDialog.setNegativeButton("No") { p0, _ ->
                p0.dismiss()
            }
            alertDialog.show()
        }

        mDb = NoteDatabase.getInstance(view.context)
        nDb = AccountDatabase.getInstance(view.context)

        binding.refreshFetch.setOnClickListener {
            fetchData()
        }

        binding.welcomeImage.setOnClickListener {
            val items = arrayOf("Clear All Notes", "Change Username", "Change Password", "About App", "Delete Account", "Logout")

            MaterialAlertDialogBuilder(it.context)
                .setTitle(resources.getString(R.string.setting))
                .setItems(items) { _, which ->
                    when (which) {
                        0 -> {
                            clearAllNotes(sharedPreference.getInt(MainActivity.LOGGED_IN_USER_ID, -1))
                        }
                        1 -> {
                            val getView = LayoutInflater.from(requireContext())
                                .inflate(R.layout.custom_dialog_change_username, null, false)
                            val dialogBuilder = AlertDialog.Builder(requireContext())
                            dialogBuilder.setView(getView)
                            dialogBuilder.setCancelable(true)

                            val customDialog = dialogBuilder.create()
                            val changeButton = getView.findViewById<Button>(R.id.change_username)

                            changeButton.setOnClickListener {
                                val newUsername = getView.findViewById<TextInputLayout>(R.id.input_new_username).editText?.text.toString()
                                val oldPassword = getView.findViewById<TextInputLayout>(R.id.input_password).editText?.text.toString()
                                val userId = sharedPreference.getInt(MainActivity.LOGGED_IN_USER_ID, -1)

                                if (newUsername.isEmpty() or oldPassword.isEmpty()) {
                                    Toast.makeText(requireContext(), "Please Fill All Blanks", Toast.LENGTH_LONG).show()
                                } else {
                                    GlobalScope.launch {
                                        val singleAccount = nDb?.AccountDao()?.getSingleInformationAccount(userId)
                                        if (singleAccount?.get(0)?.password == oldPassword) {
                                            GlobalScope.async {
                                                val result = nDb?.AccountDao()?.updateUsernameAccount(newUsername, userId)
                                                requireActivity().runOnUiThread {
                                                    if (result != 0) {
                                                        Toast.makeText(requireContext(), "Username Changed Successfully", Toast.LENGTH_LONG).show()
                                                        customDialog.dismiss()
                                                        val editor: SharedPreferences.Editor = sharedPreference.edit()
                                                        editor.putBoolean(MainActivity.LOGGED_IN, false)
                                                        editor.remove(MainActivity.LOGGED_USERNAME)
                                                        editor.apply()
                                                        view.findNavController().navigateUp()
                                                    } else {
                                                        Toast.makeText(requireContext(), "Username Change Failed", Toast.LENGTH_LONG).show()
                                                    }
                                                }
                                            }
                                        } else {
                                            requireActivity().runOnUiThread {
                                                Toast.makeText(requireContext(), "Old Password Not Match To System", Toast.LENGTH_LONG).show()
                                            }
                                        }
                                    }
                                }
                            }
                            customDialog.show()
                        }
                        2 -> {
                            val getView = LayoutInflater.from(requireContext())
                                .inflate(R.layout.custom_dialog_change_password, null, false)
                            val dialogBuilder = AlertDialog.Builder(requireContext())
                            dialogBuilder.setView(getView)
                            dialogBuilder.setCancelable(true)

                            val customDialog = dialogBuilder.create()
                            val changeButton = getView.findViewById<Button>(R.id.change_password)

                            changeButton.setOnClickListener {
                                val oldText = getView.findViewById<TextInputLayout>(R.id.input_old_password).editText?.text.toString()
                                val newText = getView.findViewById<TextInputLayout>(R.id.input_new_password).editText?.text.toString()
                                val new2Text = getView.findViewById<TextInputLayout>(R.id.input_again_new_password).editText?.text.toString()
                                val userId = sharedPreference.getInt(MainActivity.LOGGED_IN_USER_ID, -1)

                                if (oldText.isEmpty() or newText.isEmpty() or new2Text.isEmpty()) {
                                    Toast.makeText(requireContext(), "Please Fill All Blanks", Toast.LENGTH_LONG).show()
                                } else {
                                    if (new2Text == newText) {
                                        GlobalScope.launch {
                                            val singleAccount = nDb?.AccountDao()?.getSingleInformationAccount(userId)
                                            if (singleAccount?.get(0)?.password == oldText) {
                                                GlobalScope.async {
                                                    val result = nDb?.AccountDao()?.updatePasswordAccount(new2Text, userId)
                                                    requireActivity().runOnUiThread {
                                                        if (result != 0) {
                                                            Toast.makeText(requireContext(), "Password Changed Successfully", Toast.LENGTH_LONG).show()
                                                            customDialog.dismiss()
                                                            val editor: SharedPreferences.Editor = sharedPreference.edit()
                                                            editor.putBoolean(MainActivity.LOGGED_IN, false)
                                                            editor.remove(MainActivity.LOGGED_USERNAME)
                                                            editor.apply()
                                                            view.findNavController().navigateUp()
                                                        } else {
                                                            Toast.makeText(requireContext(), "Password Change Failed", Toast.LENGTH_LONG).show()
                                                        }
                                                    }
                                                }
                                            } else {
                                                requireActivity().runOnUiThread {
                                                    Toast.makeText(requireContext(), "Old Password Not Match To System", Toast.LENGTH_LONG).show()
                                                }
                                            }
                                        }
                                    } else {
                                        Toast.makeText(requireContext(), "New Password Different, Please Check Again", Toast.LENGTH_LONG).show()
                                    }
                                }
                            }
                            customDialog.show()
                        }
                        3 -> {
                            aboutApp()
                        }
                        4 -> {
                            val getView = LayoutInflater.from(requireContext())
                                .inflate(R.layout.custom_dialog_delete_account, null, false)
                            val dialogBuilder = AlertDialog.Builder(requireContext())
                            dialogBuilder.setView(getView)
                            dialogBuilder.setCancelable(true)

                            val customDialog = dialogBuilder.create()
                            val deleteButton = getView.findViewById<Button>(R.id.delete_account)

                            deleteButton.setOnClickListener {
                                val oldPassword = getView.findViewById<TextInputLayout>(R.id.input_password).editText?.text.toString()
                                val userId = sharedPreference.getInt(MainActivity.LOGGED_IN_USER_ID, -1)

                                if (oldPassword.isEmpty()) {
                                    Toast.makeText(requireContext(), "Please Fill All Blanks", Toast.LENGTH_LONG).show()
                                } else {
                                    GlobalScope.launch {
                                        val singleAccount = nDb?.AccountDao()?.getSingleInformationAccount(userId)
                                        if (singleAccount?.get(0)?.password == oldPassword) {
                                            GlobalScope.async {
                                                val checkHaveNotes = mDb?.NoteDao()?.getAllNoteUser(userId)
                                                if (checkHaveNotes!!.isEmpty()) {
                                                    val result = nDb?.AccountDao()?.deleteSingleAccount(userId)
                                                    requireActivity().runOnUiThread {
                                                        if (result != 0) {
                                                            Toast.makeText(requireContext(), "Account Deleted Successfully", Toast.LENGTH_LONG).show()
                                                            customDialog.dismiss()
                                                            val editor: SharedPreferences.Editor = sharedPreference.edit()
                                                            editor.putBoolean(MainActivity.LOGGED_IN, false)
                                                            editor.remove(MainActivity.LOGGED_USERNAME)
                                                            editor.apply()
                                                            view.findNavController().navigateUp()
                                                        } else {
                                                            Toast.makeText(requireContext(), "Account Deleted Failed", Toast.LENGTH_LONG).show()
                                                        }
                                                    }
                                                } else {
                                                    val result = nDb?.AccountDao()?.deleteSingleAccount(userId)
                                                    val result2 = mDb?.NoteDao()?.deleteAllNoteUser(userId)
                                                    requireActivity().runOnUiThread {
                                                        if (result != 0 && result2 != 0) {
                                                            Toast.makeText(requireContext(), "Account Deleted Successfully", Toast.LENGTH_LONG).show()
                                                            customDialog.dismiss()
                                                            val editor: SharedPreferences.Editor = sharedPreference.edit()
                                                            editor.putBoolean(MainActivity.LOGGED_IN, false)
                                                            editor.remove(MainActivity.LOGGED_USERNAME)
                                                            editor.apply()
                                                            view.findNavController().navigateUp()
                                                        } else {
                                                            Toast.makeText(requireContext(), "Account Deleted Failed", Toast.LENGTH_LONG).show()
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            requireActivity().runOnUiThread {
                                                Toast.makeText(requireContext(), "Old Password Not Match To System", Toast.LENGTH_LONG).show()
                                            }
                                        }
                                    }
                                }
                            }
                            customDialog.show()
                        }
                        5 -> {
                            logoutAccount()
                        }
                    }
                }
                .show()
        }

        binding.textLogout.setOnClickListener {
            logoutAccount()
        }

        binding.addNewNote.setOnClickListener {
            val getView = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog_add_new, null, false)
            val dialogBuilder = AlertDialog.Builder(requireContext())
            dialogBuilder.setView(getView)
            dialogBuilder.setCancelable(true)

            val customDialog = dialogBuilder.create()
            val saveButton = getView.findViewById<Button>(R.id.save_add)

            saveButton.setOnClickListener {
                val titleText = getView.findViewById<TextInputLayout>(R.id.input_item_title).editText?.text.toString()
                val descriptionText = getView.findViewById<TextInputLayout>(R.id.input_item_description).editText?.text.toString()
                val dateText = getView.findViewById<TextInputLayout>(R.id.input_item_date).editText?.text.toString()
                val userIdCreate = sharedPreference.getInt(MainActivity.LOGGED_IN_USER_ID, -1)

                if (titleText.isEmpty() or descriptionText.isEmpty() or dateText.isEmpty()) {
                    Toast.makeText(requireContext(), "Please Fill All Blanks", Toast.LENGTH_LONG).show()
                } else {
                    val objectNote = NoteDataClass(null, userIdCreate, titleText, descriptionText, dateText)

                    GlobalScope.async {
                        val result = mDb?.NoteDao()?.insertNote(objectNote)
                        requireActivity().runOnUiThread {
                            if (result != 0.toLong()) {
                                Toast.makeText(requireContext(), "Note Added Successfully", Toast.LENGTH_LONG).show()
                                customDialog.dismiss()
                                fetchData()
                            } else {
                                Toast.makeText(requireContext(), "Note Add Failed", Toast.LENGTH_LONG).show()
                            }
                        }
                    }
                }
            }
            customDialog.show()
        }
    }

    override fun onResume() {
        super.onResume()
        fetchData()

        var timeBack : Long = 0
        requireView().isFocusableInTouchMode = true
        requireView().requestFocus()
        requireView().setOnKeyListener { _, i, keyEvent ->
            if (keyEvent.action === KeyEvent.ACTION_UP && i == KeyEvent.KEYCODE_BACK) {
                if (System.currentTimeMillis() - timeBack > 1000) {
                    timeBack = System.currentTimeMillis()
                    Toast.makeText(requireContext(), "Press Again To Exit App", Toast.LENGTH_LONG).show()
                } else {
                    exitProcess(0)
                }
                true
            } else false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        NoteDatabase.destroyInstance()
        _binding = null
    }

    private fun clearAllNotes(userId: Int) {
        val alertDialog = AlertDialog.Builder(requireContext())
        alertDialog.setTitle("Are You Sure?")
        alertDialog.setMessage("Are You Sure Want To Clear ALl Notes On Your Account?")
        alertDialog.setPositiveButton("Yes") { p0, _ ->
            GlobalScope.async {
                val result = mDb?.NoteDao()?.deleteAllNoteUser(userId)
                requireActivity().runOnUiThread {
                    if (result != 0) {
                        Toast.makeText(requireContext(), "All Note Have Been Deleted", Toast.LENGTH_LONG).show()
                        p0.dismiss()
                        fetchData()
                    } else {
                        Toast.makeText(requireContext(), "Failed To Delete All Note", Toast.LENGTH_LONG).show()
                    }
                }
            }
        }
        alertDialog.setNegativeButton("No") { p0, _ ->
            p0.dismiss()
        }
        alertDialog.show()
    }

    private fun aboutApp() {
        val getView = LayoutInflater.from(requireContext()).inflate(R.layout.custom_dialog_about_app, null, false)
        val dialogBuilder = AlertDialog.Builder(requireContext())
        dialogBuilder.setView(getView)
        dialogBuilder.setCancelable(true)

        val customDialog = dialogBuilder.create()
        customDialog.show()
    }

    private fun fetchData() {
        GlobalScope.launch {
            val sharedPreference: SharedPreferences = requireActivity().getSharedPreferences(
                MainActivity.SHARED_PREF_FILE,
                Context.MODE_PRIVATE
            )
            val getIdUser = sharedPreference.getInt(MainActivity.LOGGED_IN_USER_ID, -1)
            val listNote = mDb?.NoteDao()?.getAllNoteUser(getIdUser)

            requireActivity().runOnUiThread {
                listNote?.let {
                    val adapter = RecyclerViewAdapter(it as ArrayList<NoteDataClass>) { its ->
                        if (its) {
                            fetchData()
                        }
                    }
                    binding.homeRecyclerView.adapter = adapter
                    if (listNote.isEmpty()) {
                        binding.showNothingTable.visibility = View.VISIBLE
                    } else {
                        binding.showNothingTable.visibility = View.GONE
                    }
                }
            }
        }
    }
}