package com.ilhamsafari.challenge4_ilhamsafari.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.ilhamsafari.challenge4_ilhamsafari.data.AccountDataClass
import com.ilhamsafari.challenge4_ilhamsafari.database.AccountDatabase
import com.ilhamsafari.challenge4_ilhamsafari.databinding.FragmentRegisterBinding
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.launch

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mDb: AccountDatabase? = AccountDatabase.getInstance(view.context)

        binding.registerButton.setOnClickListener {
            val inputUsername = binding.inputUsername.editText?.text.toString()
            val inputEmail = binding.inputEmail.editText?.text.toString()
            val inputPassword = binding.inputPassword.editText?.text.toString()
            val inputPasswordTwice = binding.inputPasswordTwice.editText?.text.toString()

            if (inputEmail.isEmpty() or inputPassword.isEmpty() or inputUsername.isEmpty() or inputPasswordTwice.isEmpty()) {
                Toast.makeText(requireContext(), "Please Fill All Blank To Register", Toast.LENGTH_LONG).show()
            } else {
                if (inputPassword == inputPasswordTwice) {
                    GlobalScope.launch {
                        val listAccount = mDb?.AccountDao()?.getEmailAccountUnique(inputEmail)
                        if (listAccount?.size != 0) {
                            requireActivity().runOnUiThread {
                                Toast.makeText(requireContext(), "Email Already In Used, Please Login Instead", Toast.LENGTH_LONG).show()
                            }
                        } else {
                            val objectAccount = AccountDataClass(null, inputUsername, inputEmail, inputPassword)

                            GlobalScope.async {
                                val result = mDb.AccountDao().insertAccount(objectAccount)
                                requireActivity().runOnUiThread {
                                    if (result != 0.toLong()) {
                                        Toast.makeText(requireContext(), "Account Registered, Please Login", Toast.LENGTH_LONG).show()
                                        it.findNavController().navigateUp()
                                    } else {
                                        Toast.makeText(requireContext(), "Account Registration Failed", Toast.LENGTH_LONG).show()
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Toast.makeText(requireContext(), "Password Difference, Please Check Again", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        AccountDatabase.destroyInstance()
        _binding = null
    }
}