package com.ilhamsafari.challenge4_ilhamsafari.fragment

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.ilhamsafari.challenge4_ilhamsafari.MainActivity
import com.ilhamsafari.challenge4_ilhamsafari.database.AccountDatabase
import com.ilhamsafari.challenge4_ilhamsafari.databinding.FragmentLoginBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mDb: AccountDatabase? = AccountDatabase.getInstance(view.context)
        val sharedPreference: SharedPreferences = requireActivity().getSharedPreferences(
            MainActivity.SHARED_PREF_FILE,
            Context.MODE_PRIVATE
        )

        val loggedInStatus = sharedPreference.getBoolean(MainActivity.LOGGED_IN, false)

        if (loggedInStatus) {
            view.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
        }

        binding.loginButton.setOnClickListener {
            val inputEmail = binding.inputEmail.editText?.text.toString()
            val inputPassword = binding.inputPassword.editText?.text.toString()

            if (inputEmail.isEmpty() or inputPassword.isEmpty()) {
                Toast.makeText(requireContext(), "Please Fill All Blank To Login", Toast.LENGTH_LONG).show()
            } else {
                GlobalScope.launch {
                    val loginAccount = mDb?.AccountDao()?.getLoginAccount(inputEmail, inputPassword)
                    if (loginAccount?.size != 0) {
                        requireActivity().runOnUiThread {
                            val editor: SharedPreferences.Editor = sharedPreference.edit()
                            editor.putBoolean(MainActivity.LOGGED_IN, true)
                            loginAccount?.get(0)?.id?.let { it1 ->
                                editor.putInt(
                                    MainActivity.LOGGED_IN_USER_ID,
                                    it1
                                )
                            }
                            editor.putString(MainActivity.LOGGED_USERNAME, loginAccount?.get(0)?.username)
                            editor.apply()
                            val showWelcomeSnackbar = Snackbar.make(it, "Welcome Back ${loginAccount?.get(0)?.username}", Snackbar.LENGTH_LONG)
                            showWelcomeSnackbar.setAction("Dismiss") {
                                showWelcomeSnackbar.dismiss()
                            }
                            showWelcomeSnackbar.show()
                            it.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToHomeFragment())
                        }
                    } else {
                        requireActivity().runOnUiThread {
                            Toast.makeText(requireContext(), "Email Or Password Wrong, Please Check Again", Toast.LENGTH_LONG).show()
                        }
                    }
                }
            }
        }

        binding.signUpText.setOnClickListener {
            it.findNavController().navigate(LoginFragmentDirections.actionLoginFragmentToRegisterFragment())
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}